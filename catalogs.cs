## Version $VER: dktronics-speech.acepansion.catalog 1.0 (11.04.2021)
## Languages english fran�ais deutsch espa�ol
## Codeset english 0
## Codeset fran�ais 0
## Codeset deutsch 0
## Codeset espa�ol 0
## SimpleCatConfig CharsPerLine 200
## Header Locale_Strings
## TARGET C english "generated/locale_strings.h" NoCode
## TARGET CATALOG fran�ais "Release/Catalogs/fran�ais/" Optimize
## TARGET CATALOG deutsch "Release/Catalogs/deutsch/" Optimize
## TARGET CATALOG espa�ol "Release/Catalogs/espa�ol/" Optimize
MSG_MENU_TOGGLE
Plug a DK'Tronics speech synthesizer
Brancher un synth�tiseur vocal DK'Tronics
DK'Tronics Sprachsynthesizer anschlie�en
Conecte un sintetizador de voz DK'Tronics
;
MSG_TITLE
DK'Tronics speech synthesizer
Synth�tiseur vocal DK'Tronics
DK'Tronics Sprachsynthesizer
Sintetizador de voz DK'Tronics
;
